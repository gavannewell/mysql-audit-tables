Generates SQL for audit tables and triggers in MySQL. Simply point the tool at an
existing MySQL database and it will generate the SQL table and trigger creation 
scripts to record all insert, update and delete actions.

This is a NetBeans Maven project with a Java Swing frontend. Run the project to 
download dependencies and display the GUI. Then enter details of your existing 
database to generate the audit tables SQL. Finally copy the generated SQL from 
the clipboard, review/edit and apply.

For example if we have a simple table as follows:

CREATE TABLE `test`.`t1` (
  `c1` INT NOT NULL AUTO_INCREMENT,
  `c2` VARCHAR(45) NOT NULL,
  `c3` DATE NULL,
  PRIMARY KEY (`c1`));

Then the tool will generate the following SQL:

-- Audit tables creation script

-- !Ups

CREATE TABLE `test`.`audit_t1` (
`audit_id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Unique system generated audit record identifier',
`audit_action` CHAR(1) CHARACTER SET 'ascii' COLLATE 'ascii_general_ci' NOT NULL COMMENT 'Action taken as either Insert, Update or Delete',
`audit_time` DATETIME NOT NULL DEFAULT NOW() COMMENT 'When the audit record was created',
`audit_user` VARCHAR(16) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL COMMENT 'MySQL database user making the change',
`c1` int(11) NOT NULL ,
`c2` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
`c3` date ,
PRIMARY KEY (`audit_id`)
) ENGINE=InnoDB DEFAULT COLLATE=latin1_swedish_ci;

DELIMITER $
CREATE DEFINER = CURRENT_USER TRIGGER `test`.`audit_t1_AFTER_INSERT` AFTER INSERT ON `test`.`t1`
FOR EACH ROW BEGIN INSERT INTO `test`.`audit_t1`
(`audit_action`, `audit_time`, `audit_user`, `c1`, `c2`, `c3`)
VALUES
('I', NOW(), SUBSTRING_INDEX(USER(),'@',1), NEW.`c1`, NEW.`c2`, NEW.`c3`);
END;
$
DELIMITER ;

DELIMITER $
CREATE DEFINER = CURRENT_USER TRIGGER `test`.`audit_t1_AFTER_UPDATE` AFTER UPDATE ON `test`.`t1`
FOR EACH ROW BEGIN INSERT INTO `test`.`audit_t1`
(`audit_action`, `audit_time`, `audit_user`, `c1`, `c2`, `c3`)
VALUES
('U', NOW(), SUBSTRING_INDEX(USER(),'@',1), NEW.`c1`, NEW.`c2`, NEW.`c3`);
END;
$
DELIMITER ;

DELIMITER $
CREATE DEFINER = CURRENT_USER TRIGGER `test`.`audit_t1_AFTER_DELETE` AFTER DELETE ON `test`.`t1`
FOR EACH ROW BEGIN INSERT INTO `test`.`audit_t1`
(`audit_action`, `audit_time`, `audit_user`, `c1`, `c2`, `c3`)
VALUES
('D', NOW(), SUBSTRING_INDEX(USER(),'@',1), OLD.`c1`, OLD.`c2`, OLD.`c3`);
END;
$
DELIMITER ;


-- !Downs

DROP TRIGGER `test`.`audit_t1_AFTER_INSERT`;
DROP TRIGGER `test`.`audit_t1_AFTER_UPDATE`;
DROP TRIGGER `test`.`audit_t1_AFTER_DELETE`;
DROP TABLE `test`.`audit_t1`;
