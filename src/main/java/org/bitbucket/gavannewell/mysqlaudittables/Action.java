
package org.bitbucket.gavannewell.mysqlaudittables;


public enum Action {
    INSERT,
    UPDATE,
    DELETE;
    
    public String getCode()
    {
        return this.name().substring(0, 1);
    }
    
    public String getOldOrNew()
    {
        if( this.equals(Action.DELETE) )
            return "OLD";
        else
            return "NEW";
    }
}
