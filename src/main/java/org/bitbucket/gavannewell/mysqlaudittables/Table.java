package org.bitbucket.gavannewell.mysqlaudittables;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Table {
    
    private final String tableName;
    private final String engine;
    private final String tableCollation;
    private final ArrayList<Field> fields = new ArrayList<Field>();
    
    public Table(ResultSet rs) throws SQLException
    {
        this.tableName = rs.getString("TABLE_NAME");
        this.engine = rs.getString("ENGINE");
        this.tableCollation = rs.getString("TABLE_COLLATION");
    }
    
    public String getTableName()
    {
        return this.tableName;
    }
    
    public ArrayList<Field> getFields()
    {
        return this.fields;
    }
    
    public String getMasterTableName(String database)
    {
        return "`" + database + "`.`" + this.tableName + "`";
    }
    
    public String getAuditTableName(String database)
    {
        return "`" + database + "`.`audit_" + this.tableName + "`";
    }
    
    public String getTriggerName(String database, Action action)
    {
        return "`" + database + "`.`audit_" + this.tableName + "_AFTER_" + action.name() + "`";
    }
    
    public String getTableCreate(String database)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE ").append(this.getAuditTableName(database)).append(" (\n");
        sb.append("`audit_id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Unique system generated audit record identifier',\n");
        sb.append("`audit_action` CHAR(1) CHARACTER SET 'ascii' COLLATE 'ascii_general_ci' NOT NULL COMMENT 'Action taken as either Insert, Update or Delete',\n");
        sb.append("`audit_time` DATETIME NOT NULL DEFAULT NOW() COMMENT 'When the audit record was created',\n");
        sb.append("`audit_user` VARCHAR(16) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL COMMENT 'MySQL database user making the change',\n");
        for( Field field : this.fields )
        {
            sb.append(field.getColumnCreate()).append("\n");
        }
        sb.append("PRIMARY KEY (`audit_id`)\n");
        sb.append(") ENGINE=").append(this.engine).append(" DEFAULT COLLATE=").append(this.tableCollation).append(";\n");
        return sb.toString();
    }
    
    public String getTableDrop(String database)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TABLE ").append(this.getAuditTableName(database)).append(";\n");
        return sb.toString();
    }
    
    public String getTriggerCreate(String database, Action action)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("DELIMITER $\n");
        sb.append("CREATE DEFINER = CURRENT_USER TRIGGER ").append(this.getTriggerName(database, action)).append(" AFTER ").append(action.name()).append(" ON ").append(this.getMasterTableName(database)).append("\n");
        sb.append("FOR EACH ROW BEGIN INSERT INTO ").append(this.getAuditTableName(database)).append("\n");
        sb.append("(`audit_action`, `audit_time`, `audit_user`, ");
        for( int i = 0; i < this.fields.size(); i++ )
        {
            Field field = this.fields.get(i);
            sb.append(field.getColumnName());
            if( i < this.fields.size() - 1 )
                sb.append(", ");
        }
        sb.append(")\n");
        sb.append("VALUES\n");
        sb.append("('").append(action.getCode()).append("', NOW(), SUBSTRING_INDEX(USER(),'@',1), ");
        for( int i = 0; i < this.fields.size(); i++ )
        {
            Field field = this.fields.get(i);
            sb.append(action.getOldOrNew()).append(".").append(field.getColumnName());
            if( i < this.fields.size() - 1 )
                sb.append(", ");
        }
        sb.append(");\n");
        sb.append("END;\n");
        sb.append("$\n");
        sb.append("DELIMITER ;\n");
        return sb.toString();
    }
    
    public String getTriggerDrop(String database, Action action)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("DROP TRIGGER ").append(this.getTriggerName(database, action)).append(";\n");
        return sb.toString();
    }
}
