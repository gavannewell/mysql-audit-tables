
package org.bitbucket.gavannewell.mysqlaudittables;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;

public class Database {
    private final String databaseName;
    private final TreeMap<String, Table> tables = new TreeMap<String, Table>();
    
    public Database(String databaseName, Connection conn) throws SQLException
    {
        this.databaseName = databaseName;
        Statement stmt = null;
        ResultSet meta = null;
        
        try
        {
            String sql = "SELECT\n" +
                " t.`TABLE_NAME`,\n" +
                " t.`ENGINE`,\n" +
                " t.`TABLE_COLLATION`,\n" +
                " c.`COLUMN_NAME`,\n" +
                " c.`DATA_TYPE`,\n" +
                " c.`CHARACTER_MAXIMUM_LENGTH`,\n" +
                " c.`NUMERIC_PRECISION`,\n" +
                " c.`NUMERIC_SCALE`,\n" +
                " c.`CHARACTER_SET_NAME`,\n" +
                " c.`COLLATION_NAME`,\n" +
                " c.`IS_NULLABLE`,\n" +
                " c.`COLUMN_TYPE`,\n" +
                " c.`COLUMN_COMMENT`\n" +
                "FROM information_schema.tables AS t\n" +
                "JOIN information_schema.columns AS c\n" +
                "ON t.TABLE_SCHEMA = c.TABLE_SCHEMA\n" +
                "AND t.TABLE_NAME = c.TABLE_NAME\n" +
                "WHERE t.TABLE_SCHEMA = '" + this.databaseName + "'\n" +
                "AND t.TABLE_TYPE = 'BASE TABLE'\n" +
                "AND t.ENGINE IN('InnoDB', 'MyISAM')\n" +
                "AND t.TABLE_NAME NOT LIKE 'audit_%'\n" +
                "ORDER BY t.`TABLE_NAME`, c.`ORDINAL_POSITION`";
                stmt = conn.createStatement();
                meta = stmt.executeQuery(sql);
            
            while( meta.next() )
            {
                final String tableName = meta.getString("TABLE_NAME");
                Table table = this.tables.get(tableName);
                if( table == null )
                {
                    table = new Table(meta);
                    tables.put(tableName, table);
                }
                table.getFields().add(new Field(meta));
            }
        }
        finally
        {
            try{ meta.close(); } catch( Exception e ) {/* ignore */}
            try{ stmt.close(); } catch( Exception e ) {/* ignore */}
        }
    }
    
    public String getSchemaCreate(String databaseName)
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("-- Create database schema\n");
        sb.append("CREATE SCHEMA `").append(databaseName).append("` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;\n");
        sb.append("\n");
        sb.append("\n");
        
        return sb.toString();
    }
    
    public String getGrantCreate(String adminUser, String normalUser, String databaseName)
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("-- Create users and grant permissions\n");
        sb.append("-- The admin user '").append(adminUser).append("' to used only to manage database structure.\n");
        sb.append("-- The normal user '").append(normalUser).append("' to used by applications.\n");
        sb.append("-- NEVER use the admin user directly in applications, otherwise security of audit tables is lost!\n");
        sb.append("\n");
        sb.append("-- Create admin user\n");
        sb.append("CREATE USER '").append(adminUser).append("' IDENTIFIED BY PASSWORD 'InsertSecretPasswordHere';\n");
        sb.append("\n");
        sb.append("-- Create normal user\n");
        sb.append("CREATE USER '").append(normalUser).append("' IDENTIFIED BY PASSWORD 'InsertSecretPasswordHere';\n");
        sb.append("\n");
        sb.append("-- Grant admin user permissions\n");
        sb.append("GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, ALTER, INDEX, DROP, TRIGGER ON `").append(databaseName).append("`.* TO '").append(adminUser).append("';\n");
        sb.append("\n");
        sb.append("-- Grant normal user permissions\n");
        sb.append("GRANT SELECT ON `").append(databaseName).append("`.* TO '").append(normalUser).append("';\n");
        for( Table table : this.tables.values() )
        {
            sb.append("GRANT INSERT, UPDATE, DELETE ON `").append(databaseName).append("`.`").append(table.getTableName()).append("` TO '").append(normalUser).append("';\n");
        }
        sb.append("\n");
        sb.append("\n");
        
        return sb.toString();
    }
    
    public String getDatabaseCreate()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append("-- Audit tables creation script\n\n");
        sb.append("-- !Ups\n\n");
        for( Table table : this.tables.values() )
        {
            sb.append(table.getTableCreate(this.databaseName));
            sb.append("\n");
        }
        
        for( Table table : this.tables.values() )
        {
            sb.append(table.getTriggerCreate(this.databaseName, Action.INSERT));
            sb.append("\n");
            sb.append(table.getTriggerCreate(this.databaseName, Action.UPDATE));
            sb.append("\n");
            sb.append(table.getTriggerCreate(this.databaseName, Action.DELETE));
            sb.append("\n");
        }
        
        sb.append("\n");
        sb.append("-- !Downs\n\n");
        for( Table table : this.tables.values() )
        {
            sb.append(table.getTriggerDrop(this.databaseName, Action.INSERT));
            sb.append(table.getTriggerDrop(this.databaseName, Action.UPDATE));
            sb.append(table.getTriggerDrop(this.databaseName, Action.DELETE));
        }
        
        for( Table table : this.tables.values() )
        {
            sb.append(table.getTableDrop(this.databaseName));
        }
        
        return sb.toString();
    }
}
