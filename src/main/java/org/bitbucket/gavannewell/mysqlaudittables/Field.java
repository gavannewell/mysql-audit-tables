
package org.bitbucket.gavannewell.mysqlaudittables;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Field {
    private final String columnName;
    private final String dataType;
    private final Long characterMaximumLength;
    private final Integer numericPrecision;
    private final Integer numericScale;
    private final String characterSetName;
    private final String collationName;
    private final Boolean isNullable;
    private final String columnComment;
    private final String columnType;
    
    public Field(ResultSet rs) throws SQLException
    {
        this.columnName = rs.getString("COLUMN_NAME");
        this.dataType = rs.getString("DATA_TYPE");
        this.characterMaximumLength = rs.getLong("CHARACTER_MAXIMUM_LENGTH");
        this.numericPrecision = rs.getInt("NUMERIC_PRECISION");
        this.numericScale = rs.getInt("NUMERIC_SCALE");
        this.characterSetName = rs.getString("CHARACTER_SET_NAME");
        this.collationName = rs.getString("COLLATION_NAME");
        this.isNullable = "YES".equalsIgnoreCase(rs.getString("IS_NULLABLE"));
        this.columnComment = rs.getString("COLUMN_COMMENT");
        this.columnType = rs.getString("COLUMN_TYPE");
    }
    
    public String getColumnName()
    {
        return "`" + this.columnName + "`";
    }
    
    public String getColumnCreate()
    {
        StringBuilder sb = new StringBuilder();
        
        sb.append('`').append(this.columnName).append('`').append(' ');
        sb.append(this.columnType).append(' ');
        if( this.characterSetName != null )
            sb.append("CHARACTER SET ").append(this.characterSetName).append(' ');
        if( this.collationName != null )
            sb.append("COLLATE ").append(this.collationName).append(' ');
        if( !this.isNullable )
            sb.append("NOT NULL ");
        sb.append(",");
        
        return sb.toString();
    }
}
